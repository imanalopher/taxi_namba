<?php

namespace App\Form;

use App\Entity\Car;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class Car1Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('number', null, array(
                'label_attr' => array('class' => 'col-sm-2 control-label'),
                'label' => 'Номер',
                'attr' => array('class' => 'form-control'),
                'translation_domain' => false
            ))
            ->add('title', null, array(
                'label_attr' => array('class' => 'col-sm-2 control-label'),
                'label' => 'Название',
                'attr' => array('class' => 'form-control'),
                'translation_domain' => false
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Car::class,
        ]);
    }
}
