<?php

namespace App\Form;

use App\Entity\Invoice;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class InvoiceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('price', null, array(
                'label_attr' => array('class' => 'col-sm-2 control-label'),
                'label' => 'Цена',
                'attr' => array('class' => 'form-control'),
                'translation_domain' => false
            ))
            ->add('title', null, array(
                'label_attr' => array('class' => 'col-sm-2 control-label'),
                'attr' => array('class' => 'form-control'),
                'label' => 'Название',
                'translation_domain' => false
            ))
            ->add('car', null, array(
                'label_attr' => array('class' => 'col-sm-2 control-label'),
                'label' => 'Авто',
                'attr' => array('class' => 'form-control'),
                'translation_domain' => false
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Invoice::class,
        ]);
    }
}
