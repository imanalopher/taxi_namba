<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class HomeController extends Controller
{
    public function index()
    {
        if(!$this->getUser())
            return $this->redirectToRoute('sto_login');
        else
            return $this->redirectToRoute('sto_invoice_index');
    }
}