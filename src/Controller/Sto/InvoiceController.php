<?php

namespace App\Controller\Sto;

use App\Entity\Invoice;
use App\Form\InvoiceType;
use App\Repository\InvoiceRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/sto/invoice")
 */
class InvoiceController extends Controller
{
    /**
     * @Route("", name="sto_invoice_index", methods="GET")
     * @param InvoiceRepository $invoiceRepository
     * @return Response
     */
    public function index(InvoiceRepository $invoiceRepository): Response
    {
        return $this->render('sto/invoice/index.html.twig', ['invoices' => $invoiceRepository->findAll()]);
    }

    /**
     * @Route("/new", name="sto_invoice_new", methods="GET|POST")
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $invoice = new Invoice();
        $form = $this->createForm(InvoiceType::class, $invoice);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($invoice);
            $em->flush();

            return $this->redirectToRoute('sto_invoice_index');
        }

        return $this->render('sto/invoice/new.html.twig', [
            'invoice' => $invoice,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="invoice_delete", methods="DELETE")
     * @param Request $request
     * @param Invoice $invoice
     * @return Response
     */
    public function delete(Request $request, Invoice $invoice): Response
    {
        if ($this->isCsrfTokenValid('delete'.$invoice->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($invoice);
            $em->flush();
        }

        return $this->redirectToRoute('sto_invoice_index');
    }
}