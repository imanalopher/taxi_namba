<?php

namespace App\Controller\Admin;

use App\Entity\Car;
use App\Form\Car1Type;
use App\Repository\CarRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/car")
 */
class CarController extends Controller
{
    /**
     * @Route("", name="admin_car_index", methods="GET")
     * @param CarRepository $carRepository
     * @return Response
     */
    public function index(CarRepository $carRepository): Response
    {
        return $this->render('admin/car/index.html.twig', ['cars' => $carRepository->findAll()]);
    }

    /**
     * @Route("/new", name="admin_car_new", methods="GET|POST")
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $car = new Car();
        $form = $this->createForm(Car1Type::class, $car);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($car);
            $em->flush();

            return $this->redirectToRoute('admin_car_index');
        }

        return $this->render('admin/car/new.html.twig', [
            'car' => $car,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="admin_car_show", methods="GET")
     * @param Car $car
     * @return Response
     */
    public function show(Car $car): Response
    {

        return $this->render('admin/car/show.html.twig', ['car' => $car]);
    }

    /**
     * @Route("/{id}/edit", name="admin_car_edit", methods="GET|POST")
     * @param Request $request
     * @param Car $car
     * @return Response
     */
    public function edit(Request $request, Car $car): Response
    {
        $form = $this->createForm(Car1Type::class, $car);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_car_index');
        }

        return $this->render('admin/car/edit.html.twig', [
            'car' => $car,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="admin_car_delete", methods="DELETE")
     * @param Request $request
     * @param Car $car
     * @return Response
     */
    public function delete(Request $request, Car $car): Response
    {
        if ($this->isCsrfTokenValid('delete'.$car->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($car);
            $em->flush();
        }

        return $this->redirectToRoute('admin_car_index');
    }
}
