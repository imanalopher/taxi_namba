<?php

namespace App\Controller\Admin;

use App\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class SecurityController extends Controller
{
    /**
     * @Route("/admin/login", name="admin_login")
     * @param Request $request
     * @return Response
     */
    public function login(Request $request)
    {
        if (!$this->getUser() && $request->isMethod('POST'))
        {
            $user = $this->getDoctrine()->getRepository(User::class)->findAdmin($request->request->get('_username'));
            if($user instanceof User) {
                $encoder = $this->container->get('security.password_encoder');

                if ($encoder->isPasswordValid($user, $request->request->get('_password'))) {
                    if ($user->hasRole('ROLE_SUPER_ADMIN')) {
                        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
                        $this->get('security.token_storage')->setToken($token);

                        $this->get('session')->set('_security_main', serialize($token));

                        $event = new InteractiveLoginEvent($request, $token);
                        $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);
                        return $this->redirectToRoute('admin_car_index');
                    }
                }
            }
        }

        return $this->render('security/login.html.twig');
    }

    /**
     * @Route("/admin", name="admin")
     * @return Response
     */
    public function admin()
    {
        return $this->render('security/login.html.twig');
    }
}